package io.springdata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
        @Autowired
        private DataSource dataSource;

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.jdbcAuthentication()
                    .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM user u WHERE u.name=?")
                    .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM us er u WHERE u.name=?")
                    .dataSource(dataSource)
                    .passwordEncoder(passwordEncoder());
        }
        @Override
        protected void configure(HttpSecurity http) throws Exception{
            http.authorizeRequests()
                    .antMatchers("/console/login.jsp?jsessionid=2c4aa922a3858c45455ef643ccaf5219").permitAll()
                    .antMatchers("/api/product").permitAll()
                    .antMatchers("/api/product/all").permitAll()
                    .antMatchers("/api/order").permitAll()
                    .antMatchers("/api/order/all").permitAll()
                    .antMatchers("/api/order/post").permitAll()
                    .antMatchers("/api/customer").hasRole("CUSTOMER")
                    .antMatchers("/api/customer/all").hasRole("CUSTOMER")
                    .antMatchers("/api/product/post").hasRole("ADMIN")
                    .antMatchers("/api/product/put").hasRole("ADMIN")
                    .antMatchers("/api/customer/post").hasRole("ADMIN")
                    .antMatchers("/api/customer/put").hasRole("ADMIN")
                    .antMatchers("/api/order/post").hasRole("ADMIN")
                    .antMatchers("/api/order/put").hasRole("ADMIN")
                    .and().formLogin().permitAll()
                    .and().logout().permitAll();
        }
}
