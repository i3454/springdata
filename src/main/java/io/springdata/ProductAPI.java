package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/product")
public class ProductAPI {
    private ProductManager manager;

    @Autowired
    public ProductAPI(ProductManager manager) {
        this.manager = manager;
    }

    @GetMapping("/all")
    public Iterable<Product> getAll(){
        return manager.findAll();
    }

    @GetMapping
    public Optional<Product> getById(@RequestParam long id){
        return manager.find(id);
    }

    @PostMapping("/post")
    public Product addProduct(@RequestBody Product product){
        return manager.save(product);
    }

    @PutMapping("/put")
    public Product updateProduct(@RequestBody Product product){
        return manager.save(product);
    }

    @DeleteMapping
    public void deleteProduct(@RequestParam long id){
        manager.deleteById(id);
    }
}
