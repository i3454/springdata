package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/order")
public class MyOrderAPI {
    private MyOrderManager manager;

    @Autowired
    public MyOrderAPI(MyOrderManager manager) {
        this.manager = manager;
    }

    @GetMapping("/all")
    public Iterable<MyOrder> getAll(){
        return manager.findAll();
    }

    @GetMapping
    public Optional<MyOrder> getById(@RequestParam long id){
        return manager.find(id);
    }

    @PostMapping("/post")
    public MyOrder addOrder(@RequestBody MyOrder order){
        return manager.save(order);
    }

    @PutMapping("/put")
    public MyOrder updateOrder(@RequestBody MyOrder order){
        return manager.save(order);
    }

    @DeleteMapping
    public void deleteOrder(@RequestParam long id){
        manager.deleteById(id);
    }
}
