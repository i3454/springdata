package io.springdata;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyOrderRepo extends CrudRepository<MyOrder, Long> {
}