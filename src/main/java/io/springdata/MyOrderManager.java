package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyOrderManager {
    private MyOrderRepo repo;

    @Autowired
    public MyOrderManager(MyOrderRepo repo) {
        this.repo = repo;
    }

    public Optional<MyOrder> find(Long id) {
        return repo.findById(id);
    }

    public Iterable<MyOrder> findAll() {
        return  repo.findAll();
    }

    public MyOrder save(MyOrder myOrder) {
        return  repo.save(myOrder);
    }

    public void deleteById(Long id){
        repo.deleteById(id);
    }
}
