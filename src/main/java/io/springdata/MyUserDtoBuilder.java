package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
@Component

public class MyUserDtoBuilder {
    @Autowired
    private PasswordEncoder passwordEncoder;

    public MyUserDto transform(MyUser myUser) {

        return new MyUserDto(myUser.getName(), passwordEncoder.encode(myUser.getPassword()), myUser.getRole());
    }

}
