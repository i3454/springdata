package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
public class CustomerAPI {
    private CustomerManager manager;

    @Autowired
    public CustomerAPI(CustomerManager manager) {
        this.manager = manager;
    }

    @GetMapping("/all")
    public Iterable<Customer> getAll(){
        return manager.findAll();
    }

    @GetMapping
    public Optional<Customer> getById(@RequestParam long id){
        return manager.find(id);
    }

    @PostMapping("/post")
    public Customer addCustomer(@RequestBody Customer customer){
        return manager.save(customer);
    }

    @PutMapping("/put")
    public Customer updateCustomer(@RequestBody Customer customer){
        return manager.save(customer);
    }

    @DeleteMapping
    public void deleteCustomer(@RequestParam long id){
        manager.deleteById(id);
    }
}
