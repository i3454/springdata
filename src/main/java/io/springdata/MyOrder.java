package io.springdata;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class MyOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Customer customer;
    @ManyToMany
    private Set<Product> set;
    private LocalDateTime placedate;
    private String status;

    public MyOrder() {
    }

    public MyOrder(Customer customer, Set<Product> set, LocalDateTime placedate, String status) {
        this.customer = customer;
        this.set = set;
        this.placedate = placedate;
        this.status = status;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setSet(Set<Product> set) {
        this.set = set;
    }

    public void setPlacedate(LocalDateTime placedate) {
        this.placedate = placedate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Set<Product> getSet() {
        return set;
    }

    public LocalDateTime getPlacedate() {
        return placedate;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

}
