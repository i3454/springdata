package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerManager {
    private CustomerRepo repo;

    @Autowired
    public CustomerManager(CustomerRepo repo) {
        this.repo = repo;
    }

    public Optional<Customer> find(Long id) {
        return repo.findById(id);
    }

    public Iterable<Customer> findAll() {
        return  repo.findAll();
    }

    public Customer save(Customer customer) {
        return  repo.save(customer);
    }

    public void deleteById(Long id){
        repo.deleteById(id);
    }
}
