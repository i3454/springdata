package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDtoManager {
    private MyUserDtoRepo repo;
    @Autowired
    public MyUserDtoManager(MyUserDtoRepo repo) {
        this.repo = repo;
    }

    public Optional<MyUserDto> findByName(String name) {
        return repo.findById(name);
    }

    public Iterable<MyUserDto> findAll() {
        return  repo.findAll();
    }

    public MyUserDto save(MyUserDto myUserDto) {
        return  repo.save(myUserDto);
    }
}
