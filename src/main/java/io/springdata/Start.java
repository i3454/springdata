package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collections;

@Component
public class Start {

    private ProductRepo productRepo;
    private CustomerRepo customerRepo;
    private MyOrderRepo myOrderRepo;
    private MyUserDtoBuilder builder;
    private MyUserDtoRepo myUserDtoRepo;

    @Autowired
    public Start(ProductRepo productRepo, CustomerRepo customerRepo, MyOrderRepo myOrderRepo, MyUserDtoBuilder builder, MyUserDtoRepo myUserDtoRepo ) {
        this.productRepo = productRepo;
        this.customerRepo = customerRepo;
        this.myOrderRepo = myOrderRepo;
        this.myUserDtoRepo = myUserDtoRepo;
        this.builder = builder;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runExample() {
        Product product = new Product("8h Snu", (float) 10000,false);
        productRepo.save(product);

        Customer customer = new Customer("Zofia Dobrowolska","Wrocław");
        customerRepo.save(customer);

        MyOrder order = new MyOrder(customer, Collections.singleton(product), LocalDateTime.now(),"brak w magazynie");
        myOrderRepo.save(order);

        MyUser myUser = new MyUser("Joe Ordinary", "password", "CUSTOMER");
        MyUser myAdmin = new MyUser("Admin", "Admin123", "ADMIN");

        MyUserDto userDto = builder.transform(myUser);
        MyUserDto adminDto = builder.transform(myAdmin);

        myUserDtoRepo.save(userDto);
        myUserDtoRepo.save(adminDto);
    }

}