package io.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductManager {
    private ProductRepo repo;

    @Autowired
    public ProductManager(ProductRepo repo) {
        this.repo = repo;
    }

    public Optional<Product> find(Long id) {
        return repo.findById(id);
    }

    public Iterable<Product> findAll() {
        return  repo.findAll();
    }

    public Product save(Product product) {
        return  repo.save(product);
    }

    public void deleteById(Long id){
        repo.deleteById(id);
    }
}
